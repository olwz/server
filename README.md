# StayTuned API v1

![node](https://img.shields.io/badge/Node-6.5.9-green.svg?style=flat-square)
![ECMAScript](https://img.shields.io/badge/ECMAScript-ES2016-green.svg?style=flat-square)

API v1 . Build with Node and express


StayTuned API location @ (http://54.87.22.128:8080)
---------------------------------------

Common. Utility functions used internally.
------------------------------------------

    api.fetch('auth/fb', {options}); // fetch + API endpoint (IpAddress)

    /// Upload an object, the options have the account reerence
    api.upload('photo', {options});

    api.authenticate('method', {data}); // 'mehtod' is the type of authetication, 'basic'or 'facebook'

    api.isAuthenticated(); // Returnt the auth status, -> (bool)

    api.logout(); // 😱


Authentication
--------------

[x]  auth/fb
[x] Authenticate the user or register if is a new user.
  - { fbID, image, email } → { code: x, message, errors:[elements_errors] }
[x]  auth/basic
[x] Authenticate
  - { email, password } - Login → { code: x, message, errors:[elements_errors] }
[x] Register
  - { email, password, image } → { code: x, message, error }


[x] user/profile

Feed Options
------------

The application support two types of feed:
Allow the user search for a job or search for someone to work.

The user should be able to navigate in a map, and see the jobs in an specific region.
All the api calls will include the user position or his gps coordinates.

jobs/offer
----------

- [x] {padding, offset}
- [x] GET → retrieve the last 50 posts
- [x] POST → {…} // Publish a new job - This will be an user offering his services.
- [ ] Support options to filter
- [x] filter by category
- [ ] filter by location
- [?] filter by gps position (????)

jobs/request
------------

- [x] {padding, offset}
- [x] GET → retrieve the last 50 posts
- [x] POST → {…} // Publish a job request. Basically this will a person looking for someone to perform a task.
- [ ] Support options to filter
- [x] filter by category
- [ ] filter by location
- [?] filter by gps position (????)
