'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _models = require('../models');

var _models2 = _interopRequireDefault(_models);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mainRouter = _express2.default.Router();

// models import
// Import node module


// for path files


// for testing
var faker = require('faker');
var parse = require('csv-parse');
var fs = require('fs');

// inserting locations and parties to the database
mainRouter.get('/insert', function (req, res) {
    insert(req.headers.size);
    res.json({
        message: 'data inserted'
    });
});

// inserting locations and parties to the database
mainRouter.delete('/parties', function (req, res) {
    _models2.default.Party.sync({
        force: true
    }).then(function () {
        res.send("party database deleted");
    });
});

// inserting locations and parties to the database
mainRouter.delete('/users', function (req, res) {
    _models2.default.User.sync({
        force: true
    }).then(function () {
        res.send("user database deleted");
    });
});

function insert(tam) {
    var i = 0;

    var _loop = function _loop() {

        var tamanho = Math.floor(Math.random() * 5);

        var gallery = [faker.image.nightlife() + ('?' + Math.round(Math.random() * 45)), 'https://rocklintrends.files.wordpress.com/2010/08/1171.jpg', faker.image.nightlife()];

        _models2.default.sequelize.sync().then(function () {
            //saving userId and its token
            _models2.default.Party.create({
                userId: 3,
                image: faker.image.avatar(),
                partyImage: faker.image.nightlife(),
                title: faker.name.jobTitle(),
                author: faker.name.firstName() + " " + faker.name.lastName(),
                city: faker.address.city(),
                state: faker.address.stateAbbr(),
                latitude: faker.address.latitude(),
                longitude: faker.address.longitude(),
                dateOfParty: faker.date.month() + " " + Math.floor(Math.random() * 31),
                startTime: "20:00",
                endTime: "23:00",
                date: new Date(),
                description: faker.lorem.paragraph(),
                private: faker.random.boolean(),
                email: faker.internet.email(),
                telephone: faker.phone.phoneNumber(),
                gallery: gallery
            });
        });
    };

    for (i = 0; i < tam; i++) {
        _loop();
    }
}

// Exporting an object as the default import for this module
exports.default = mainRouter;
//# sourceMappingURL=main.routes.js.map
