'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _models = require('../models');

var _models2 = _interopRequireDefault(_models);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ACTION CODES


// Import bluebird
// Import node module
var PARTY_POSTED = {
    code: 0,
    message: 'party posted'
};

// importing the models from sequelize


// to decode jsonwebtoken

var PARTY_NOT_FOUND = {
    code: 1,
    message: 'party not found'
};
var PARTY_DELETED = {
    code: 2,
    message: 'party deleted'
};
var INVALID_PARAMETERS = {
    code: 3,
    message: 'invalid parameters'
};
var INVALID_TOKEN = {
    code: 4,
    message: 'invalid token'
};
var COULD_NOT_DECODE = {
    code: 5,
    message: 'could not decode token'
};
var PARTY_ASSIGNED = {
    code: 6,
    message: 'party successful assigned to user'
};

// the router for the parties
var partiesRouter = _express2.default.Router();

/* Party routes and helpers  */

// get the parties
partiesRouter.get('/user', function (req, res, next) {

    if (req.headers.token) {
        (function () {

            var decoded = _jsonwebtoken2.default.decode(req.headers.token); // we decode the token

            // if it is decoded
            if (decoded) {

                // We look if the token is valid
                isTokenValid(decoded, req.headers.token).then(function (valid) {
                    if (valid) {
                        // if token is valid

                        queryUserParties(decoded.userId).then(function (parties) {
                            res.json(parties);
                        });
                    } else {
                        res.status(401).json(INVALID_TOKEN);
                    }
                });
            } else {
                res.status(401).json(COULD_NOT_DECODE);
            }
        })();
    } else {
        res.status(401).json(INVALID_PARAMETERS);
    }
});

// get the parties
partiesRouter.get('/', function (req, res, next) {
    queryParties(req.query).then(function (parties) {
        res.json(parties);
    });
});

// post the party parties
partiesRouter.post('/', function (req, res) {

    if (req.headers.token) {
        (function () {

            var decoded = _jsonwebtoken2.default.decode(req.headers.token); // we decode the token

            // if it is decoded
            if (decoded) {

                // We look if the token is valid
                isTokenValid(decoded, req.headers.token).then(function (valid) {
                    if (valid) {
                        // if token is valid

                        // gets the owner of the post
                        _models2.default.User.findOne({
                            where: {
                                id: decoded.userId
                            },
                            raw: true
                        }).then(function (user) {

                            /* the party to be saved */
                            var newParty = req.body;
                            newParty['userId'] = decoded.userId; // userId for future reference
                            newParty['email'] = user.email; // the user's email
                            newParty['telephone'] = user.telephone; // the user's phone
                            newParty['author'] = user.alias; // the author's alias
                            newParty['image'] = user.image; // the author's avatar

                            saveParty(newParty).then(function (saved) {
                                res.json(PARTY_POSTED);
                            });
                        });
                    } else {
                        res.status(401).json(INVALID_TOKEN);
                    }
                });
            } else {
                res.status(401).json(COULD_NOT_DECODE);
            }
        })();
    } else {
        res.status(401).json(INVALID_PARAMETERS);
    }
});

//User go to party
partiesRouter.post('/assignUsers', function (req, res) {

    if (req.headers.token) {
        (function () {

            var decoded = _jsonwebtoken2.default.decode(req.headers.token); // we decode the token

            // if it is decoded
            if (decoded) {

                // We look if the token is valid
                isTokenValid(decoded, req.headers.token).then(function (valid) {
                    if (valid) {
                        // if token is valid
                        if (decoded.userId && req.body.party_id) {
                            userPartyToggle({
                                user_id: decoded.userId,
                                party_id: req.body.party_id,
                                status: req.body.status
                            }).then(function (saved) {
                                res.json(PARTY_ASSIGNED);
                            });
                        } else {
                            res.status(401).json(INVALID_PARAMETERS);
                        }
                    } else {
                        res.status(401).json(INVALID_TOKEN);
                    }
                });
            } else {
                res.status(401).json(COULD_NOT_DECODE);
            }
        })();
    } else {
        res.status(401).json(INVALID_PARAMETERS);
    }
});

// Get a party by its id
partiesRouter.get('/:id', function (req, res) {
    if (req.params.id) {
        findParty(req.params.id).then(function (party) {
            if (party) {
                res.json(party);
            } else {
                res.status(401).json(PARTY_NOT_FOUND);
            };
        });
    } else {
        res.status(401).json(INVALID_PARAMETERS);
    }
});

// Get a party by its id
partiesRouter.post('/getParty', function (req, res) {
    if (req.headers.token) {
        (function () {
            var decoded = _jsonwebtoken2.default.decode(req.headers.token); // we decode the token

            // if it is decoded
            if (decoded) {

                // We look if the token is valid
                isTokenValid(decoded, req.headers.token).then(function (valid) {
                    if (valid) {
                        // if token is valid
                        if (decoded.userId && req.body.party_id) {
                            findParty(req.body.party_id).then(function (party) {

                                if (!party) return res.status(401).json(PARTY_NOT_FOUND);

                                findPartyUser({
                                    userId: decoded.userId,
                                    partyId: req.body.party_id
                                }).then(function (user) {
                                    if (user) {
                                        party.dataValues.status = true;
                                    } else {
                                        party.dataValues.status = false;
                                    }
                                    findFriends({
                                        userId: decoded.userId,
                                        partyId: req.body.party_id
                                    }).then(function (friends) {
                                        var activeFriend = [];
                                        _bluebird2.default.map(friends, function (friend) {
                                            return findPartyUser({
                                                userId: friend.dataValues.friend_id,
                                                partyId: req.body.party_id
                                            }).then(function (done) {
                                                if (done) {
                                                    activeFriend.push({
                                                        id: friend.dataValues.facebook_id,
                                                        name: friend.dataValues.name
                                                    });
                                                }
                                            });
                                        }).then(function () {
                                            party.dataValues.friends = activeFriend;
                                            res.json(party);
                                        });
                                    });
                                });
                            });
                        } else {
                            res.status(401).json(INVALID_PARAMETERS);
                        }
                    } else {
                        res.status(401).json(INVALID_TOKEN);
                    }
                });
            } else {
                res.status(401).json(COULD_NOT_DECODE);
            }
        })();
    } else {
        res.status(401).json(INVALID_PARAMETERS);
    }
});

// Get a party by its id
partiesRouter.delete('/:id', function (req, res) {
    if (req.params.id) {
        deleteParty(req.params.id).then(function (party) {
            if (party) {
                res.json(PARTY_DELETED);
            } else {
                res.status(401).json(PARTY_NOT_FOUND);
            };
        });
    } else {
        res.status(401).json(INVALID_PARAMETERS);
    }
});

/* query for parties using some parameters to filter them*/
function queryParties(query) {

    /* the where clause*/
    var where = {
        private: false
    };
    if (query.city) {
        where.city = {
            $iLike: '%' + query.city + '%'
        };
    } else {
        where.city = {
            $ne: null
        };
    };
    if (query.state) {
        where.state = {
            $iLike: '%' + query.state + '%'
        };
    } else {
        where.state = {
            $ne: null
        };
    };

    /* query for the parties */
    return _models2.default.Party.findAll({
        where: where,
        raw: true,
        attributes: {
            exclude: ['updatedAt']
        },
        limit: query.limit || 50,
        offset: query.offset || 0,
        order: [['createdAt', 'DESC']] // get the latest parties ordered by creation
    });
}

/* query for user parties*/
function queryUserParties(userId) {

    /* the where clause*/
    var where = {
        userId: userId
    };

    /* query for the parties */
    return _models2.default.Party.findAll({
        where: where,
        raw: true,
        attributes: {
            exclude: ['updatedAt']
        },
        order: [['createdAt', 'DESC']] // get the latest parties ordered by creation
    });
}

// save a party to the database
function saveParty(party) {
    return _models2.default.sequelize.sync().then(function () {
        //saving userId and its token
        return _models2.default.Party.create({
            author: party.author,
            userId: party.userId,
            dateOfParty: party.dateOfParty,
            description: party.description,
            email: party.email,
            image: party.image,
            location: party.location,
            title: party.title,
            latitude: party.latitude,
            longitude: party.longitude,
            city: party.city,
            state: party.state,
            telephone: party.telephone,
            gallery: party.gallery,
            private: party.private,
            date: new Date(),
            startTime: party.startTime,
            endTime: party.endTime
        });
    });
}

// get a specific party
function findParty(id) {
    return _models2.default.Party.findOne({
        where: {
            id: id
        }
    });
}

// get a specific party
function deleteParty(id) {
    return _models2.default.Party.destroy({
        where: {
            id: id
        }
    });
}

// check if a token is valid
function isTokenValid(decoded, token) {
    return _models2.default.Token.findOne({
        where: {
            userId: decoded.userId,
            token: token
        }
    }).then(function (token) {
        if (!token) {
            return false;
        } else {
            return true;
        }
    });
}

//Check user party status
function userPartyToggle(body) {
    if (body.status) {
        return _models2.default.UsersParties.create({
            user_id: body.user_id,
            party_id: body.party_id
        });
    } else {
        return _models2.default.UsersParties.destroy({
            where: {
                user_id: body.user_id,
                party_id: body.party_id
            }
        });
    }
}

//Check user party user
function findPartyUser(body) {
    return _models2.default.UsersParties.findOne({
        where: {
            user_id: body.userId,
            party_id: body.partyId
        }
    });
}

//Check user party user
function findFriends(body) {
    return _models2.default.UserFreinds.findAll({
        where: {
            user_id: body.userId.toString()
        }
    });
}

// Exporting an object as the default import for this module
exports.default = partiesRouter;
//# sourceMappingURL=parties.routes.js.map
