'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.facebook = facebook;

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _models = require('../models');

var _models2 = _interopRequireDefault(_models);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// the router for the auth


// For hashing passwords


// import bluebird
var authRouter = _express2.default.Router();

// Action CODES


// models


// For web Token
// import express
var USER_FOUND = {
    code: 0,
    message: 'user found'
};
var USER_NOT_FOUND = {
    code: 1,
    message: 'user not found'
};
var WRONG_PASSWORD = {
    code: 2,
    message: 'wrong password'
};
var WRONG_PARAMETERS = {
    code: 3,
    message: 'wrong parameters'
};
var INVALID_PARAMETERS = {
    code: 4,
    message: 'invalid parameters'
};

// login facebook
function facebook() {

    // POST Connect with facebook
    return authRouter.post('/fb', function (req, res, next) {

        // verify for the parameters
        if (req.body.facebookId && req.body.image) {

            // looks for user by facebook id
            findUserByConditon({
                facebookId: req.body.facebookId
            }).then(function (user) {
                if (user) {
                    // if found
                    deleteFriends(user.id).then(function (done) {
                        saveFriend(user.id, req.body.friends).then(function () {
                            res.json({
                                code: 0,
                                image: user.image,
                                token: generateToken(user),
                                errors: []
                            }); // return token to client
                        });
                    });
                } else {
                    // else generates a new user
                    generateUser(req.body).then(function (user) {
                        deleteFriends(user.id).then(function (done) {
                            saveFriend(user.id, req.body.friends).then(function () {
                                res.json({
                                    code: 0,
                                    image: user.image,
                                    token: generateToken(user),
                                    errors: []
                                }); // return token to client
                            });
                        });
                    });
                }
            });
        } else {

            // error array
            var errors = [];

            // returns the error code specific for the missing parameter
            if (!req.body.facebookId) errors.push('facebookId');
            if (!req.body.image) errors.push('image');

            // add the erros to the response
            INVALID_PARAMETERS['errors'] = errors;

            //send the response
            res.status(400).json(INVALID_PARAMETERS);
        }
    });
}

authRouter.post('/signup', function (req, res) {
    if (req.body.email && req.body.password) {
        isEmailValid(req.body.email).then(function (user) {
            if (user) return res.status(400).json(USER_FOUND);
            generateUser(req.body).then(function (user) {
                res.json({
                    code: 0,
                    image: user.image,
                    token: generateToken(user),
                    errors: []
                });
            });
        });
    } else {
        return res.status(400).json(INVALID_PARAMETERS);
    }
});

authRouter.post('/signIn', function (req, res) {
    if (req.body.email && req.body.password) {
        findUserByConditon({
            email: req.body.email,
            password: generatePassword(req.body.password)
        }).then(function (user) {
            if (user) {
                // if found
                res.json({
                    code: 0,
                    image: user.image,
                    token: generateToken(user),
                    errors: []
                }); // return token to client
            } else {
                return res.status(400).json(USER_NOT_FOUND);
            }
        });
    } else {
        return res.status(400).json(INVALID_PARAMETERS);
    }
});

// generateUser
function generateUser(body) {
    return _models2.default.sequelize.sync().then(function () {
        //saving userId and its token
        return _models2.default.User.create({
            facebookId: body.facebookId || null,
            password: generatePassword(body.password),
            email: body.email,
            image: body.image,
            alias: body.alias
        });
    });
}

// generates token by facebook id
function generateToken(user) {

    //user has authenticated correctly thus we create a JWT token
    var userToken = _jsonwebtoken2.default.sign({
        facebookId: user.facebookId,
        userId: user.id
    }, 'getcash-api', {
        algorithm: 'HS512'
    });

    _models2.default.sequelize.sync().then(function () {
        //saving userId and its token
        _models2.default.Token.create({
            userId: user.id,
            token: userToken
        });
    });

    return userToken;
}
// generate password hash
function generatePassword(password) {
    if (password) {
        return _crypto2.default.createHash('SHA256').update(password).digest("hex");
    } else {
        return null;
    }
}

// find user in the database
function findUserByConditon(condition) {
    return _models2.default.User.findOne({
        where: condition
    });
}

//Check email validation
function isEmailValid(email) {
    return _models2.default.User.findOne({
        where: {
            email: email
        }
    });
}

//Save users friends
function saveFriend(userId, friendList) {
    return _bluebird2.default.map(friendList, function (friend) {
        findUserByFbId(friend.id).then(function (user) {
            return _models2.default.sequelize.sync().then(function () {
                //saving userId and its token
                return _models2.default.UserFreinds.create({
                    user_id: userId,
                    friend_id: user.id,
                    facebook_id: friend.id,
                    name: friend.name
                });
            });
        });
    });
}

// find user in the database
function findUserByFbId(id) {
    return _models2.default.User.findOne({
        where: {
            facebookId: id
        }
    });
}

// Remove user friends
function deleteFriends(userId) {
    return _models2.default.UserFreinds.destroy({
        where: {
            user_id: userId
        }
    });
}

// checks if a token exists
/*function isTokenValid(user){
  return models.Token.findOne({
    where:{
      userId: user.id
    }
  });
}*/
//# sourceMappingURL=auth.js.map
