"use strict";

module.exports = function(sequelize, DataTypes) {
  var Party = sequelize.define("Party", {
    userId: {
      type: DataTypes.INTEGER,
      references: {
        model: "Users",
        key: "id"
      },
      onDelete: 'cascade',
    },
    author: DataTypes.STRING,
    dateOfParty: DataTypes.STRING,
    startTime: DataTypes.STRING,
    endTime: DataTypes.STRING,
    partyImage: DataTypes.STRING,
    date: DataTypes.DATE,
    description: DataTypes.TEXT,
    email: DataTypes.STRING,
    telephone: DataTypes.STRING,
    image: DataTypes.STRING,
    latitude: DataTypes.FLOAT,
    longitude: DataTypes.FLOAT,
    city: DataTypes.STRING,
    state: DataTypes.STRING,
    title: DataTypes.STRING,
    gallery: DataTypes.ARRAY(DataTypes.STRING),
    private: DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {}
    }
  });

  Party.sync().then(function() {});

  return Party;
};
