"use strict";

module.exports = function(sequelize, DataTypes) {
  var UserFreinds = sequelize.define("UserFreinds", {
    user_id: DataTypes.STRING,
    friend_id: DataTypes.STRING,
    facebook_id: DataTypes.STRING,
    image: DataTypes.STRING,
    name: DataTypes.STRING,
  }, {
    classMethods: {
      associate: function(models) {

      }
    }
  });

  UserFreinds.sync().then(function() {});

  return UserFreinds;
};
