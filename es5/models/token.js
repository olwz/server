"use strict";

module.exports = function(sequelize, DataTypes) {
  var Token = sequelize.define("Token", {
    userId: {
      type: DataTypes.INTEGER,
      references: {
        model: "Users",
        key: "id"
      },
      onDelete: 'cascade'
    },
    token: DataTypes.STRING

  }, {
    classMethods: {
      associate: function(models) {}
    }
  });

  Token.sync().then(function() {});
    
  return Token;
};
