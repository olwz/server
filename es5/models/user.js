"use strict";

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define("User", {
    email: DataTypes.STRING,
    image: DataTypes.STRING,
    facebookId: DataTypes.STRING,
    city: DataTypes.STRING,
    state: {
      type: DataTypes.STRING
    },
    alias: DataTypes.STRING,
    password: DataTypes.STRING,
    state: DataTypes.STRING,
    telephone: DataTypes.STRING,
    resetKey: DataTypes.STRING,
  }, {
    classMethods: {
      associate: function(models) {

      }
    }
  });

  User.sync().then(function() {});

  return User;
};
