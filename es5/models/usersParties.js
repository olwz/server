"use strict";

module.exports = function(sequelize, DataTypes) {
  var UsersParties = sequelize.define("UsersParties", {
    user_id: DataTypes.INTEGER(),
    party_id: DataTypes.INTEGER(),
  }, {
    classMethods: {
      associate: function(models) {

      }
    }
  });

  UsersParties.sync().then(function() {});

  return UsersParties;
};
