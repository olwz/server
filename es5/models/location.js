"use strict";

module.exports = function(sequelize, DataTypes) {
  var Location = sequelize.define("Location", {
    zipcode: {
      type: DataTypes.INTEGER,
      unique:true
    },
    latitude: DataTypes.FLOAT,
    longitude: DataTypes.FLOAT,
    state: DataTypes.STRING,
    city: DataTypes.STRING,
    country: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {}
    }
  });

  Location.sync().then(function() {});

  return Location;
};
