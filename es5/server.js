'use strict';

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _expressMailer = require('express-mailer');

var _expressMailer2 = _interopRequireDefault(_expressMailer);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _main = require('./routes/main.routes');

var _main2 = _interopRequireDefault(_main);

var _user = require('./routes/user.routes');

var _user2 = _interopRequireDefault(_user);

var _parties = require('./routes/parties.routes');

var _parties2 = _interopRequireDefault(_parties);

var _auth = require('./inspr_modules/auth');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import cors

// app and server definition
// import body parser
var app = (0, _express2.default)(); // import mailer

// import express
// server creation

app.server = _http2.default.createServer(app);

// CORS using
app.use((0, _cors2.default)({
    credentials: true
}));
app.all('/', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

// Importing routes files


// auth and register


// for rendering htmls
app.use("/public", _express2.default.static(__dirname + '/public'));

// BodyParser for request
app.use(_bodyParser2.default.json()); // for parsing application/json

// Routes for the application
app.use('/', _main2.default);
app.use('/users', _user2.default);
app.use('/parties', _parties2.default);

// auth and register routes
app.use('/auth', (0, _auth.facebook)());
//app.use('/auth', basicLogin());
//app.use('/register', basicRegister()); // no more registers


// server listening to requests
app.server.listen(process.env.PORT || 8080, function () {
    console.log('Listening on ' + app.server.address().port);
});

_expressMailer2.default.extend(app, {
    from: 'anawahi.staff@gmail.com',
    host: 'smtp.gmail.com', // hostname
    secureConnection: true, // use SSL
    port: 465, // port for secure SMTP
    transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
    auth: {
        user: 'anawahi.staff@gmail.com',
        pass: 'anawahi2016'
    }
});

//The 404 Route (ALWAYS Keep this as the last route)
app.all('*', function (req, res) {
    res.status(404).json({
        '404': '404 error'
    });
});
//# sourceMappingURL=server.js.map
