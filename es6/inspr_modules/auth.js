// import express
import express from 'express';

// import bluebird
import Promise from 'bluebird';

// For web Token
import jwt from 'jsonwebtoken';

// For hashing passwords
import bCrypt from 'crypto';

// models
import models from '../models';

// the router for the auth
var authRouter = express.Router();

// Action CODES
let USER_FOUND = {
    code: 0,
    message: 'user found'
};
let USER_NOT_FOUND = {
    code: 1,
    message: 'user not found'
};
let WRONG_PASSWORD = {
    code: 2,
    message: 'wrong password'
};
let WRONG_PARAMETERS = {
    code: 3,
    message: 'wrong parameters'
};
let INVALID_PARAMETERS = {
    code: 4,
    message: 'invalid parameters'
};

// login facebook
export function facebook() {

    // POST Connect with facebook
    return authRouter.post('/fb', function(req, res, next) {

        // verify for the parameters
        if (req.body.facebookId && req.body.image) {

            // looks for user by facebook id
            findUserByConditon({
                facebookId: req.body.facebookId
            }).then((user) => {
                if (user) { // if found
                    deleteFriends(user.id).then((done) => {
                        saveFriend(user.id, req.body.friends).then(() => {
                            res.json({
                                code: 0,
                                image: user.image,
                                token: generateToken(user),
                                errors: []
                            }); // return token to client
                        })
                    })
                } else {
                    // else generates a new user
                    generateUser(req.body).then((user) => {
                        deleteFriends(user.id).then((done) => {
                            saveFriend(user.id, req.body.friends).then(() => {
                                res.json({
                                    code: 0,
                                    image: user.image,
                                    token: generateToken(user),
                                    errors: []
                                }); // return token to client
                            })
                        })
                    });
                }
            });

        } else {

            // error array
            let errors = []

            // returns the error code specific for the missing parameter
            if (!req.body.facebookId) errors.push('facebookId');
            if (!req.body.image) errors.push('image');

            // add the erros to the response
            INVALID_PARAMETERS['errors'] = errors;

            //send the response
            res.status(400).json(INVALID_PARAMETERS);
        }

    });
}

authRouter.post('/signup', (req, res) => {
    if (req.body.email && req.body.password) {
        isEmailValid(req.body.email).then((user) => {
            if (user) return res.status(400).json(USER_FOUND);
            generateUser(req.body).then((user) => {
                res.json({
                    code: 0,
                    image: user.image,
                    token: generateToken(user),
                    errors: []
                });
            });
        })
    } else {
        return res.status(400).json(INVALID_PARAMETERS);
    }
});

authRouter.post('/signIn', (req, res) => {
    if (req.body.email && req.body.password) {
        findUserByConditon({
            email: req.body.email,
            password: generatePassword(req.body.password)
        }).then((user) => {
            if (user) { // if found
                res.json({
                    code: 0,
                    image: user.image,
                    token: generateToken(user),
                    errors: []
                }); // return token to client
            } else {
                return res.status(400).json(USER_NOT_FOUND);
            }
        });
    } else {
        return res.status(400).json(INVALID_PARAMETERS);
    }
});

// generateUser
function generateUser(body) {
    return models.sequelize.sync().then(() => { //saving userId and its token
        return models.User.create({
            facebookId: body.facebookId || null,
            password: generatePassword(body.password),
            email: body.email,
            image: body.image,
            alias: body.alias
        })
    });
}

// generates token by facebook id
function generateToken(user) {

    //user has authenticated correctly thus we create a JWT token
    var userToken = jwt.sign({
        facebookId: user.facebookId,
        userId: user.id
    }, 'getcash-api', {
        algorithm: 'HS512'
    });

    models.sequelize.sync().then(() => { //saving userId and its token
        models.Token.create({
            userId: user.id,
            token: userToken
        });
    });

    return userToken;
}
// generate password hash
function generatePassword(password) {
    if (password) {
        return bCrypt.createHash('SHA256').update(password).digest("hex")
    } else {
        return null
    }
}

// find user in the database
function findUserByConditon(condition) {
    return models.User.findOne({
        where: condition
    });
}

//Check email validation
function isEmailValid(email) {
    return models.User.findOne({
        where: {
            email: email
        }
    })
}

//Save users friends
function saveFriend(userId, friendList) {
    return Promise.map(friendList, function(friend) {
        findUserByFbId(friend.id).then((user) => {
            return models.sequelize.sync().then(() => { //saving userId and its token
                return models.UserFreinds.create({
                    user_id: userId,
                    friend_id: user.id,
                    facebook_id: friend.id,
                    name: friend.name
                })
            });
        })
    })
}

// find user in the database
function findUserByFbId(id) {
    return models.User.findOne({
        where: {
            facebookId: id,
        }
    })
}

// Remove user friends
function deleteFriends(userId) {
    return models.UserFreinds.destroy({
        where: {
            user_id: userId
        }
    })
}

// checks if a token exists
/*function isTokenValid(user){
  return models.Token.findOne({
    where:{
      userId: user.id
    }
  });
}*/