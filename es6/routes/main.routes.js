// Import node module
import express from 'express';
let mainRouter = express.Router();

// models import
import models from '../models';

// for path files
import path from 'path'

// for testing
var faker = require('faker');
var parse = require('csv-parse');
var fs = require('fs');

// inserting locations and parties to the database
mainRouter.get('/insert', (req, res) => {
    insert(req.headers.size);
    res.json({
        message: 'data inserted'
    });
});

// inserting locations and parties to the database
mainRouter.delete('/parties', (req, res) => {
    models.Party.sync({
        force: true
    }).then(() => {
        res.send("party database deleted")
    })
});

// inserting locations and parties to the database
mainRouter.delete('/users', (req, res) => {
    models.User.sync({
        force: true
    }).then(() => {
        res.send("user database deleted")
    })
});

function insert(tam) {
    let i = 0;
    for (i = 0; i < tam; i++) {

        let tamanho = Math.floor(Math.random() * 5);

        let gallery = [faker.image.nightlife() + `?${Math.round(Math.random()*(45))}`,
            'https://rocklintrends.files.wordpress.com/2010/08/1171.jpg',
            faker.image.nightlife()
        ]

        models.sequelize.sync().then(() => { //saving userId and its token
            models.Party.create({
                userId: 3,
                image: faker.image.avatar(),
                partyImage: faker.image.nightlife(),
                title: faker.name.jobTitle(),
                author: faker.name.firstName() + " " + faker.name.lastName(),
                city: faker.address.city(),
                state: faker.address.stateAbbr(),
                latitude: faker.address.latitude(),
                longitude: faker.address.longitude(),
                dateOfParty: faker.date.month() + " " + Math.floor(Math.random() * 31),
                startTime: "20:00",
                endTime: "23:00",
                date: new Date(),
                description: faker.lorem.paragraph(),
                private: faker.random.boolean(),
                email: faker.internet.email(),
                telephone: faker.phone.phoneNumber(),
                gallery: gallery
            });

        });
    }
}


// Exporting an object as the default import for this module
export default mainRouter;