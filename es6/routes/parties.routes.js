// Import node module
import express from 'express';

// to decode jsonwebtoken
import jwt from 'jsonwebtoken';

// Import bluebird
import Promise from 'bluebird';

// importing the models from sequelize
import models from '../models';

// ACTION CODES
let PARTY_POSTED = {
    code: 0,
    message: 'party posted'
};
let PARTY_NOT_FOUND = {
    code: 1,
    message: 'party not found'
};
let PARTY_DELETED = {
    code: 2,
    message: 'party deleted'
};
let INVALID_PARAMETERS = {
    code: 3,
    message: 'invalid parameters'
};
let INVALID_TOKEN = {
    code: 4,
    message: 'invalid token'
};
let COULD_NOT_DECODE = {
    code: 5,
    message: 'could not decode token'
};
let PARTY_ASSIGNED = {
    code: 6,
    message: 'party successful assigned to user'
};


// the router for the parties
var partiesRouter = express.Router();

/* Party routes and helpers  */

// get the parties
partiesRouter.get('/user', (req, res, next) => {

    if (req.headers.token) {

        let decoded = jwt.decode(req.headers.token); // we decode the token

        // if it is decoded
        if (decoded) {

            // We look if the token is valid
            isTokenValid(decoded, req.headers.token).then((valid) => {
                if (valid) { // if token is valid

                    queryUserParties(decoded.userId).then((parties) => {
                        res.json(parties);
                    });

                } else {
                    res.status(401).json(INVALID_TOKEN);
                }
            });
        } else {
            res.status(401).json(COULD_NOT_DECODE);
        }
    } else {
        res.status(401).json(INVALID_PARAMETERS);
    }

});


// get the parties
partiesRouter.get('/', (req, res, next) => {
    queryParties(req.query).then((parties) => {
        res.json(parties);
    });
});

// post the party parties
partiesRouter.post('/', (req, res) => {

    if (req.headers.token) {

        let decoded = jwt.decode(req.headers.token); // we decode the token

        // if it is decoded
        if (decoded) {

            // We look if the token is valid
            isTokenValid(decoded, req.headers.token).then((valid) => {
                if (valid) { // if token is valid

                    // gets the owner of the post
                    models.User.findOne({
                        where: {
                            id: decoded.userId
                        },
                        raw: true
                    }).then((user) => {

                        /* the party to be saved */
                        let newParty = req.body;
                        newParty['userId'] = decoded.userId; // userId for future reference
                        newParty['email'] = user.email; // the user's email
                        newParty['telephone'] = user.telephone; // the user's phone
                        newParty['author'] = user.alias; // the author's alias
                        newParty['image'] = user.image; // the author's avatar

                        saveParty(newParty).then((saved) => {
                            res.json(PARTY_POSTED);
                        })

                    });
                } else {
                    res.status(401).json(INVALID_TOKEN);
                }
            });
        } else {
            res.status(401).json(COULD_NOT_DECODE);
        }
    } else {
        res.status(401).json(INVALID_PARAMETERS);
    }
});

//User go to party
partiesRouter.post('/assignUsers', (req, res) => {

    if (req.headers.token) {

        let decoded = jwt.decode(req.headers.token); // we decode the token

        // if it is decoded
        if (decoded) {

            // We look if the token is valid
            isTokenValid(decoded, req.headers.token).then((valid) => {
                if (valid) { // if token is valid
                    if (decoded.userId && req.body.party_id) {
                        userPartyToggle({
                            user_id: decoded.userId,
                            party_id: req.body.party_id,
                            status: req.body.status
                        }).then((saved) => {
                            res.json(PARTY_ASSIGNED);
                        })
                    } else {
                        res.status(401).json(INVALID_PARAMETERS);
                    }
                } else {
                    res.status(401).json(INVALID_TOKEN);
                }
            });
        } else {
            res.status(401).json(COULD_NOT_DECODE);
        }
    } else {
        res.status(401).json(INVALID_PARAMETERS);
    }
});

// Get a party by its id
partiesRouter.get('/:id', (req, res) => {
    if (req.params.id) {
        findParty(req.params.id).then((party) => {
            if (party) {
                res.json(party)
            } else {
                res.status(401).json(PARTY_NOT_FOUND)
            };
        });
    } else {
        res.status(401).json(INVALID_PARAMETERS);
    }
});

// Get a party by its id
partiesRouter.post('/getParty', (req, res) => {
    if (req.headers.token) {
        let decoded = jwt.decode(req.headers.token); // we decode the token

        // if it is decoded
        if (decoded) {

            // We look if the token is valid
            isTokenValid(decoded, req.headers.token).then((valid) => {
                if (valid) { // if token is valid
                    if (decoded.userId && req.body.party_id) {
                        findParty(req.body.party_id).then((party) => {

                            if (!party) return res.status(401).json(PARTY_NOT_FOUND);

                            findPartyUser({
                                userId: decoded.userId,
                                partyId: req.body.party_id
                            }).then((user) => {
                                if (user) {
                                    party.dataValues.status = true
                                } else {
                                    party.dataValues.status = false
                                }
                                findFriends({
                                    userId: decoded.userId,
                                    partyId: req.body.party_id
                                }).then((friends) => {
                                    var activeFriend = []
                                    Promise.map(friends, function(friend) {
                                        return findPartyUser({
                                            userId: friend.dataValues.friend_id,
                                            partyId: req.body.party_id
                                        }).then((done) => {
                                            if(done){
                                                activeFriend.push({
                                                    id: friend.dataValues.facebook_id,
                                                    name: friend.dataValues.name
                                                })
                                            }
                                        })
                                    }).then(() => {
                                        party.dataValues.friends = activeFriend;
                                        res.json(party)
                                    })
                                })
                            });
                        });
                    } else {
                        res.status(401).json(INVALID_PARAMETERS);
                    }
                } else {
                    res.status(401).json(INVALID_TOKEN);
                }
            });
        } else {
            res.status(401).json(COULD_NOT_DECODE);
        }
    } else {
        res.status(401).json(INVALID_PARAMETERS);
    }
});

// Get a party by its id
partiesRouter.delete('/:id', (req, res) => {
    if (req.params.id) {
        deleteParty(req.params.id).then((party) => {
            if (party) {
                res.json(PARTY_DELETED)
            } else {
                res.status(401).json(PARTY_NOT_FOUND)
            };
        });
    } else {
        res.status(401).json(INVALID_PARAMETERS);
    }
});

/* query for parties using some parameters to filter them*/
function queryParties(query) {

    /* the where clause*/
    let where = {
        private: false
    };
    if (query.city) {
        where.city = {
            $iLike: '%' + query.city + '%'
        }
    } else {
        where.city = {
            $ne: null
        }
    };
    if (query.state) {
        where.state = {
            $iLike: '%' + query.state + '%'
        }
    } else {
        where.state = {
            $ne: null
        }
    };

    /* query for the parties */
    return models.Party.findAll({
        where: where,
        raw: true,
        attributes: {
            exclude: ['updatedAt']
        },
        limit: query.limit || 50,
        offset: query.offset || 0,
        order: [
                ['createdAt', 'DESC']
            ] // get the latest parties ordered by creation
    });
}

/* query for user parties*/
function queryUserParties(userId) {

    /* the where clause*/
    let where = {
        userId: userId
    };

    /* query for the parties */
    return models.Party.findAll({
        where: where,
        raw: true,
        attributes: {
            exclude: ['updatedAt']
        },
        order: [
                ['createdAt', 'DESC']
            ] // get the latest parties ordered by creation
    });
}

// save a party to the database
function saveParty(party) {
    return models.sequelize.sync().then(() => { //saving userId and its token
        return models.Party.create({
            author: party.author,
            userId: party.userId,
            dateOfParty: party.dateOfParty,
            description: party.description,
            email: party.email,
            image: party.image,
            location: party.location,
            title: party.title,
            latitude: party.latitude,
            longitude: party.longitude,
            city: party.city,
            state: party.state,
            telephone: party.telephone,
            gallery: party.gallery,
            private: party.private,
            date: new Date(),
            startTime: party.startTime,
            endTime: party.endTime
        });
    });
}

// get a specific party
function findParty(id) {
    return models.Party.findOne({
        where: {
            id: id
        }
    });
}

// get a specific party
function deleteParty(id) {
    return models.Party.destroy({
        where: {
            id: id
        }
    });
}

// check if a token is valid
function isTokenValid(decoded, token) {
    return models.Token.findOne({
        where: {
            userId: decoded.userId,
            token: token
        }
    }).then((token) => {
        if (!token) {
            return false
        } else {
            return true
        }
    });
}

//Check user party status
function userPartyToggle(body) {
    if (body.status) {
        return models.UsersParties.create({
            user_id: body.user_id,
            party_id: body.party_id
        })
    } else {
        return models.UsersParties.destroy({
            where: {
                user_id: body.user_id,
                party_id: body.party_id
            }
        })
    }
}

//Check user party user
function findPartyUser(body) {
    return models.UsersParties.findOne({
        where: {
            user_id: body.userId,
            party_id: body.partyId
        }
    })
}

//Check user party user
function findFriends(body) {
    return models.UserFreinds.findAll({
        where: {
            user_id: body.userId.toString()
        }
    })
}


// Exporting an object as the default import for this module
export default partiesRouter;