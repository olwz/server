// Import node module
import express from 'express';

// to decode the token
import jwt from 'jsonwebtoken';

// to sent email
import nodemailer from 'nodemailer';

// import the class models
import models from '../models';

// For hashing passwords
import bCrypt from 'crypto';

// the router for the users
let userRouter = express.Router();

//ACTION CODES
let INVALID_PARAMETERS = {
    code: 3,
    message: 'invalid parameters'
};
let INVALID_TOKEN = {
    code: 4,
    message: 'invalid token'
};
let COULD_NOT_DECODE = {
    code: 7,
    message: 'could not decode token'
};

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'qwzlinfo@gmail.com',
        pass: 'test1234@A'
    }
});

// User users
userRouter.get('/me', (req, res) => {

    if (req.headers.token) {

        // we decode the token
        let decoded = jwt.decode(req.headers.token);

        // if is decoded
        if (decoded) {

            // We look if the token is valid
            isTokenValid(decoded, req.headers.token).then((valid) => {
                if (valid) {
                    findUserByAttributes({
                        id: decoded.userId
                    }).then((user) => {
                        res.json(user)
                    });
                } else {
                    res.status(401).json(INVALID_TOKEN);
                }
            });

        } else {
            res.json(COULD_NOT_DECODE);
        }
    } else {
        res.json(INVALID_PARAMETERS);
    }
});


// Edits user info
userRouter.put('/me', (req, res) => {

    if (req.headers.token) {

        // we decode the token
        let decoded = jwt.decode(req.headers.token);

        // if is decoded
        if (decoded) {

            // We look if the token is valid
            isTokenValid(decoded, req.headers.token).then((valid) => {
                if (valid) {
                    updateUser({
                        id: decoded.userId
                    }, req.body).then((result) => {
                        res.json({
                            code: 0,
                            message: 'user updated successfuly'
                        });
                    });
                } else {
                    res.status(401).json(INVALID_TOKEN);
                }
            });

        } else {
            res.json(COULD_NOT_DECODE);
        }
    } else {
        res.json(INVALID_PARAMETERS);
    }
});

// Change password
userRouter.post('/resetPassword', (req, res) => {
    if (req.body.email) {
        findUserByAttributes({
            email: req.body.email.toLowerCase()
        }).then((user) => {
            if (user) {
                // generate code
                var code = generateResetKey()

                // setup email data with unicode symbols
                let mailOptions = {
                    from: '<qwzlinfo@gmail.com>', // sender address
                    to: user.email, // list of receivers
                    subject: 'Reset your password in OWZL', // Subject line
                    html: '<b>Code ' + code + '</b>' // html body
                };

                // send mail with defined transport object
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        return res.status(400).json(error);
                    }
                    updateUser({
                        email: user.dataValues.email
                    }, {
                        resetKey: code
                    }).then((result) => {
                        res.json({
                            code: 0,
                            message: 'user updated successfuly'
                        });
                    });
                });
            } else {
                res.status(400).json(INVALID_PARAMETERS);
            }
        });
    } else {
        res.status(400).json(INVALID_PARAMETERS);
    }
});

// check reset key
userRouter.get('/checkKey', function(req, res) {
    if (req.query.key) {
        findUserByAttributes({
            resetKey: req.query.key
        }).then((user) => {
            if (user) {
                res.json({
                    code: 0,
                    message: 'Reset Key existing'
                });

            } else {
                res.status(400).json(INVALID_PARAMETERS);
            }
        });
    } else {
        res.status(400).json(INVALID_PARAMETERS);
    }
});

// update password
userRouter.put('/updatePassowrd', function(req, res) {
    if (req.body.key && req.body.password) {
        findUserByAttributes({
            resetKey: req.body.key
        }).then((user) => {
            if (user) {
                updateUser({
                    email: user.dataValues.email
                }, {
                    password: generatePassword(req.body.password),
                    resetKey: ""
                }).then((result) => {
                    let mailOptions = {
                        from: '<qwzlinfo@gmail.com>', // sender address
                        to: user.dataValues.email, // list of receivers
                        subject: 'Confirm email', // Subject line
                        html: '<b>Welcome Back</b>' // html body
                    };

                    // send mail with defined transport object
                    transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                            return res.status(400).json(error);
                        }

                        res.json({
                            code: 0,
                            message: 'user updated successfuly'
                        });
                    });
                });
            } else {
                res.status(400).json(INVALID_PARAMETERS);
            }
        });
    } else {
        res.status(400).json(INVALID_PARAMETERS);
    }
});



// check if a token is valid
function isTokenValid(decoded, token) {
    return models.Token.findOne({
        where: {
            userId: decoded.userId,
            token: token
        }
    }).then((token) => {
        if (!token) {
            return false
        } else {
            return true
        }
    });
}

// find user in the database
function findUserByAttributes(attributes) {
    return models.User.findOne({
        where: attributes,
        attributes: {
            exclude: ['createdAt', 'updatedAt', 'id', 'resetKey'],
        }
    })
}

// find user in the database
function updateUser(condition, update) {
    return models.User.findOne({
        where: condition
    }).then((user) => {

        /* patching the attributes*/
        if (update.email) {
            user.email = update.email
        }
        if (update.city) {
            user.city = update.city
        }
        if (update.alias) {
            user.alias = update.alias
        }
        if (update.telephone) {
            user.telephone = update.telephone
        }
        if (update.image) {
            user.image = update.image
        }
        if (update.facebookId) {
            user.facebookId = update.facebookId
        }
        if (update.resetKey) {
            user.resetKey = update.resetKey
        }

        if (update.password) {
            user.password = update.password
        }

        user.save({}).then((saved) => {});

    });
}

// generate password hash
function generatePassword(password) {
    if (password) {
        return bCrypt.createHash('SHA256').update(password).digest("hex")
    } else {
        return null
    }
}

//Generate UniqueCode
function generateResetKey() {
    var text = "";
    var possible = "0123456789";

    for (var i = 0; i < 4; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

// Exporting an object as the default import for this module
export default userRouter;