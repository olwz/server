// server creation
import http from 'http';
import express from 'express'; // import express
import bodyParser from 'body-parser'; // import body parser
import mailer from 'express-mailer'; // import mailer

import cors from 'cors'; // import cors

// app and server definition
var app = express();
app.server = http.createServer(app);

// CORS using
app.use(cors({
    credentials: true
}));
app.all('/', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

// Importing routes files
import mainRouter from './routes/main.routes';
import userRouter from './routes/user.routes';
import partiesRouter from './routes/parties.routes';

// auth and register
import {
    facebook
} from './inspr_modules/auth';

// for rendering htmls
app.use("/public", express.static(__dirname + '/public'));

// BodyParser for request
app.use(bodyParser.json()); // for parsing application/json

// Routes for the application
app.use('/', mainRouter);
app.use('/users', userRouter);
app.use('/parties', partiesRouter);

// auth and register routes
app.use('/auth', facebook());
//app.use('/auth', basicLogin());
//app.use('/register', basicRegister()); // no more registers


// server listening to requests
app.server.listen(process.env.PORT || 8080, function() {
    console.log('Listening on ' + app.server.address().port)
});


mailer.extend(app, {
    from: 'anawahi.staff@gmail.com',
    host: 'smtp.gmail.com', // hostname
    secureConnection: true, // use SSL
    port: 465, // port for secure SMTP
    transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
    auth: {
        user: 'anawahi.staff@gmail.com',
        pass: 'anawahi2016'
    }
});

//The 404 Route (ALWAYS Keep this as the last route)
app.all('*', function(req, res) {
    res.status(404).json({
        '404': '404 error'
    });
});